// @generated automatically by Diesel CLI.

diesel::table! {
    registrations (email) {
        username -> Nullable<Text>,
        name -> Text,
        email -> Text,
        alergies -> Text,
        datetime -> BigInt,
        payed -> Bool,
        helfikon -> Nullable<Bool>,
        notes -> Nullable<Text>,
    }
}

diesel::table! {
    tokens (username, token) {
        username -> Text,
        token -> Text,
        timestamp -> BigInt,
    }
}

diesel::table! {
    users (username) {
        username -> Text,
        email -> Text,
    }
}

diesel::joinable!(tokens -> users (username));

diesel::allow_tables_to_appear_in_same_query!(registrations, tokens, users,);
