// SPDX-FileCopyrightText: 2021 Jonah Brüchert <jbb@kaidan.im>
// SPDX-FileCopyrightText: 2022 Jonah Brüchert <jbb@kaidan.im>
//
// SPDX-License-Identifier: AGPL-3.0-only
// SPDX-License-Identifier: AGPL-3.0-or-later

// Rocket
use rocket::fairing::Fairing;
use rocket_dyn_templates::tera::{self, Value};
use rocket_dyn_templates::{Engines, Template};

// Chrono
use chrono::prelude::DateTime;
use chrono::Utc;

// std
use std::collections::HashMap;
use std::time::Duration;
use std::time::SystemTime;

pub struct TemplateExtensions {}

impl TemplateExtensions {
    pub fn filter_format_msecs_since_epoch(
        value: &Value,
        _: &HashMap<String, Value>,
    ) -> tera::Result<Value> {
        match value.as_u64() {
            Some(secs) => {
                let time = SystemTime::UNIX_EPOCH + Duration::from_secs(secs);
                let timestr = DateTime::<Utc>::from(time)
                    .format("%d %b %Y %H:%M %Z")
                    .to_string();
                Ok(Value::String(timestr))
            }
            None => Err(tera::Error::msg(
                "Provided value is not an unsigned integer".to_string(),
            )),
        }
    }

    pub fn mailto(value: &Value, _: &HashMap<String, Value>) -> tera::Result<Value> {
        match value.as_array() {
            Some(emails) => Ok(Value::String(format!(
                "mailto:?bcc={}",
                emails
                    .iter()
                    .filter_map(Value::as_str)
                    .collect::<Vec<_>>()
                    .join(",")
            ))),
            None => Err(tera::Error::msg(
                "Provided value is not an unsigned integer".to_string(),
            )),
        }
    }

    pub fn fairing() -> impl Fairing {
        Template::custom(|engines: &mut Engines| {
            engines.tera.register_filter(
                "format_msecs_since_epoch",
                TemplateExtensions::filter_format_msecs_since_epoch,
            );
            engines
                .tera
                .register_filter("mailto", TemplateExtensions::mailto);
        })
    }
}
