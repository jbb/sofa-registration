// SPDX-FileCopyrightText: 2022 Jonah Brüchert <jbb@kaidan.im>
//
// SPDX-License-Identifier: AGPL-3.0-only

use crate::schema::*;

use rocket::serde::Serialize;
use rocket::FromForm;
use rocket_sync_db_pools::{database, diesel};

use diesel::prelude::*;

#[database("sofa")]
pub struct SoFaDb(diesel::SqliteConnection);

#[derive(Insertable, Queryable)]
pub struct User {
    pub username: String,
    pub email: String,
}

#[derive(FromForm)]
pub struct RegistrationForm {
    pub username: String,
    pub name: String,
    pub email: String,
    pub alergies: String,
    pub helfikon: bool,
    pub notes: String,
}

#[derive(Insertable, Queryable, Serialize)]
#[serde(crate = "rocket::serde")]
pub struct Registration {
    pub username: Option<String>,
    pub name: String,
    pub email: String,
    pub alergies: String,
    pub datetime: i64,
    pub payed: bool,
    pub helfikon: Option<bool>,
    pub notes: Option<String>,
}

impl SoFaDb {
    pub async fn get_user(&self, username: String) -> QueryResult<User> {
        self.run(move |c| {
            users::table
                .filter(users::username.eq(username))
                .get_result(c)
        })
        .await
    }

    pub async fn save_registration(&self, registration: Registration) -> QueryResult<()> {
        self.run(move |c| {
            diesel::insert_into(registrations::table)
                .values(registration)
                .execute(c)
        })
        .await?;
        Ok(())
    }

    pub async fn get_registrations(&self) -> QueryResult<Vec<Registration>> {
        self.run(move |c| {
            registrations::table
                .order_by(registrations::datetime.asc())
                .get_results(c)
        })
        .await
    }

    pub async fn set_payed(&self, email: String, payed: bool) -> QueryResult<()> {
        self.run(move |c| {
            diesel::update(registrations::table)
                .filter(registrations::email.eq(email))
                .set(registrations::payed.eq(payed))
                .execute(c)
        })
        .await?;
        Ok(())
    }

    pub async fn delete(&self, email: String) -> QueryResult<()> {
        self.run(move |c| {
            diesel::delete(registrations::table)
                .filter(registrations::email.eq(email))
                .execute(c)
        })
        .await?;
        Ok(())
    }

    pub async fn get_all_emails(&self) -> QueryResult<Vec<String>> {
        self.run(move |c| registrations::table.select(registrations::email).load(c))
            .await
    }

    pub async fn get_unpaid_emails(&self) -> QueryResult<Vec<String>> {
        self.run(move |c| {
            registrations::table
                .filter(registrations::payed.eq(false))
                .select(registrations::email)
                .load(c)
        })
        .await
    }

    pub async fn get_paid_emails(&self) -> QueryResult<Vec<String>> {
        self.run(move |c| {
            registrations::table
                .filter(registrations::payed.eq(true))
                .select(registrations::email)
                .load(c)
        })
        .await
    }
}
