-- SPDX-FileCopyrightText: 2022 Jonah Brüchert <jbb@kaidan.im>
--
-- SPDX-License-Identifier: AGPL-3.0-only

-- Your SQL goes here
CREATE TABLE users (
    username TEXT PRIMARY KEY NOT NULL,
    email TEXT NOT NULL
);
CREATE TABLE tokens (
    username TEXT NOT NULL,
    token TEXT NOT NULL,
    PRIMARY KEY (username, token),
    FOREIGN KEY (username) REFERENCES users(username)
);
CREATE TABLE registrations (
    username TEXT,
    name TEXT NOT NULL,
    email TEXT PRIMARY KEY NOT NULL,
    alergies TEXT NOT NULL,
    datetime BIGINT NOT NULL,
    payed BOOLEAN NOT NULL,
    FOREIGN KEY (username) REFERENCES users(username)
);
